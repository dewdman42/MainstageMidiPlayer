/********************************************************************
 * MidiPlayer.js = MainstageMidPlayer
 *
 * Version: 1.0.3
 *
 * This script can be used to playback midi sequences.  Sequence data 
 * must be provided, via copy and paste into the script in a section
 * below that is commented.  This data is a CSV text format as produced
 * by the command line tool called midicsv.
 *
 * https://www.fourmilab.ch/webtools/midicsv/
 *
 * Scripter provides a GUI to select which midi channel to play out
 * of the midifile data, or ALL channels (default)
 *
 ********************************************************************/


/****************************************************************
 ****************** Paste Sequence Data here ********************
 ******************         as midicsv       ********************
 ****************************************************************/

/****************************************************************
 *
 * How to configure midi data, see here:
 *
 *  https://gitlab.com/dewdman42/MainstageMidiPlayer/-/wikis/home
 *
 ****************************************************************/




// var MidiSequence = ``;




/*****************************************************
 *****************************************************
 ***            DO NOT EDIT BELOW HERE             ***
 *****************************************************
 *****************************************************/


var NeedsTimingInfo = true;

/*******************************
 * global vars
 *******************************/

var idx = 0;
var wasPlaying = false;
var started=false;
var bootstrap=true;

/**************************************
 * CALLBACKS
 **************************************/

function ProcessMIDI() {

    handleBootstrap();

    if(started==false) {
        return;
    }
    
    /* Get current timing info */
    var info = GetTimingInfo();

    // PLAYING
    if (info.playing) {

        if(!wasPlaying) {
            chase(info);
            wasPlaying = true;
        }

        // Send all midi events from the sequence within the process block
        SendBlockEvents(info);
    }

    // NOT PLAYING - get out
    else {

        // if it was playing before, just stopped, clean up hanging notes
        if (wasPlaying) {
            MIDI.allNotesOff();
            wasPlaying = false;
        }

        started = false;
    }
}

function Reset() {
    started=true;
    wasPlaying = false;
    idx = 0;
}

/******************************************************
 * called only once right when the script is first run
 * the first time ProcessBlock is called
 * sets the play states.
 ******************************************************/

function handleBootstrap() {

    if(bootstrap) {
        let info = GetTimingInfo();
        if(info.playing) {
            started=true;
            isPlaying=true;
        }
        bootstrap=false;
    }
}

/****************************************************************
 * chase through the midiBuffer to get idx pointed to first
 * event within the current process block
 ****************************************************************/

function chase(info) {

    if(midiBuffer.length < 1) return;
    
    idx = 0;
    let ev = midiBuffer[idx];

    while( idx < midiBuffer.length
            && ev.beatPos < info.blockStartBeat) {
        idx++;
        if(idx < midiBuffer.length) {
            ev = midiBuffer[idx];
        }
    }

    // if no more events, or past the desired end beat, then msg done
    if ( idx>=midiBuffer.length) {
        console.log("Sequence Done");
    }
}

/****************************************************************
 * send midi eventse in the current process block
 ****************************************************************/

function SendBlockEvents(info) {

    // if sequence is already done, then nothing needed here
    if (idx >= midiBuffer.length) {
        return;
    }

    let ev = midiBuffer[idx];

    while (idx < midiBuffer.length
            && ev.beatPos <= info.blockEndBeat) {

        if (isChannelOn(ev.channel)) {
            ev.sendAtBeat(ev.beatPos);
        }
        idx++;
        if(idx < midiBuffer.length) {
            ev = midiBuffer[idx];
        }
        
    }
}


/**************************************
 * send all notes off for channel
 **************************************/

var ccOff = new ControlChange;
ccOff.number = 123;
ccOff.value = 0;

function sendAllNotesOff(channel) {
    ccOff.channel = channel;
    ccOff.send();
}


/**************************************
 * Look at GUI to see if channel is ON
 **************************************/

function isChannelOn(chan) {

    let menu = GuiParameter(0);

    if (menu == 0 || menu == chan ) {
        return true;
    }
    else {
        return false;
    }
}


/**************************************
 * GUI
 **************************************/

var PluginParameters = [];

PluginParameters.push({
    name: "Channel",
    type: "menu",
    valueStrings: ["ALL","1","2","3","4","5","6","7","8",
                   "9","10","11","12","13","14","15","16"],
    defaultValue: 0,
    disableAutomation: true,
    hidden: false
});


/*********************************
 * Fast GUI Access
 *********************************/

function GuiParameter(id) {

    // just in case programmer error
    if (id >= PluginParameters.length) return undefined;

    // if script was recently initialized, reload GUI value
    if (PluginParameters[id].data == undefined) {
        PluginParameters[id].data = GetParameter(id);
    }

    return PluginParameters[id].data;
}

/**************************************
 * ParameterChanged callback for GUI
 **************************************/

function ParameterChanged(id, val) {
    // TODO, might need to refine this.
    if(id == 0 && val != PluginParameters[0].data && wasPlaying) {
        sendAllNotesOff(PluginParameters[0].data);
        PluginParameters[id].data = val;
    }
}

/**************************************
 * Bootstrap Initialize Callback
 **************************************/

var PPQN = 0;
var midiBuffer = [];

function Initialize() {
    if( typeof MidiSequence == 'undefined'
            || !(typeof MidiSequence === 'string' 
               || MidiSequence instanceof String)) return;

    let csv = MidiSequence.split(/\r?\n/);
         
    // Generate actual Event objects, add in place to the midiBuffer array
    for (let i = 0; i < csv.length; i++) {
        
        // skip blank lines and comments
        if(csv[i].trim().length < 1 
                || csv[i].substr(0,1) == "#") continue;
        
        // split the fields
        let tokens = csv[i].split(",");
               
        switch(tokens[2].trim().toUpperCase()) {
        case "NOTE_ON_C":
            addNoteOn(tokens);
            break;
        case "NOTE_OFF_C":
            addNoteOff(tokens);
            break;
        case "CONTROL_C":
            addControlChange(tokens);
            break;
        case "PITCH_BEND_C":
            addPitchBend(tokens);
            break;
        case "PROGRAM_C":
            addProgramChange(tokens);
            break;
        case "CHANNEL_AFTERTOUCH_C":
            addChannelPressure(tokens);
            break;
        case "POLY_AFTERTOUCH_C":
            addPolyPressure(tokens);
            break;

        case "HEADER":
            PPQN = parseInt(tokens[5]);
            break;

        default:
            // headers and other stuff
            break;

        } // end switch
    } // end loop                  


    // Just in case, sort by beatPos, particulatly needed
    // for type 1 and 2 midifiles.

    midiBuffer.sort(function(a, b) {return a.beatPos - b.beatPos});
    delete MidiSequence;
    MidiSequence = undefined;
}



// TODO, should do better error checking in case user
//       tries to manually create CSV text

function addNoteOn(csv) {
    let obj = new NoteOn;
    obj.beatPos  = ticks2Beats(csv[1]);
    obj.channel  = MIDI.normalizeChannel( parseInt(csv[3])+1 );
    obj.pitch    = MIDI.normalizeData   ( parseInt(csv[4]) ); 
    obj.velocity = MIDI.normalizeData   ( parseInt(csv[5]) );
    obj.articulationID = normalizeArtId ( csv, 6);
    obj.port     = normalizePort        ( csv, 7);
    midiBuffer.push(obj);
}

function addNoteOff(csv) {
    let obj = new NoteOff;
    obj.beatPos  = ticks2Beats(csv[1]);
    obj.channel  = MIDI.normalizeChannel( parseInt(csv[3])+1 );
    obj.pitch    = MIDI.normalizeData   ( parseInt(csv[4]) ); 
    obj.velocity = MIDI.normalizeData   ( parseInt(csv[5]) );
    obj.articulationID = normalizeArtId ( csv, 6);
    obj.port     = normalizePort        ( csv, 7);
    midiBuffer.push(obj);
}

function addControlChange(csv) {
    let obj = new ControlChange;
    obj.beatPos = ticks2Beats(csv[1]);
    obj.channel = MIDI.normalizeChannel( parseInt(csv[3])+1 );
    obj.number  = MIDI.normalizeData   ( parseInt(csv[4]) ); 
    obj.value   = MIDI.normalizeData   ( parseInt(csv[5]) );
    obj.articulationID = normalizeArtId ( csv, 6);
    obj.port     = normalizePort        ( csv, 7);
    midiBuffer.push(obj);
}

function addPitchBend(csv) {
    let obj = new PitchBend;
    obj.beatPos = ticks2Beats(csv[1]);
    obj.channel = MIDI.normalizeChannel( parseInt(csv[3])+1 );
    obj.value   = normalizePitchBend( csv[4]);
    obj.articulationID = normalizeArtId ( csv, 5);
    obj.port     = normalizePort        ( csv, 6);
    midiBuffer.push(obj);
}

function addProgramChange(csv) {
    let obj = new ProgramChange;
    obj.beatPos = ticks2Beats(csv[1]);
    obj.channel = MIDI.normalizeChannel( parseInt(csv[3])+1 );
    obj.number  = MIDI.normalizeData   ( parseInt(csv[4]) ); 
    obj.articulationID = normalizeArtId ( csv, 5);
    obj.port     = normalizePort        ( csv, 6);
    midiBuffer.push(obj);
}

function addChannelPressure(csv) {
    let obj = new ChannelPressure;
    obj.beatPos = ticks2Beats(csv[1]);
    obj.channel = MIDI.normalizeChannel( parseInt(csv[3])+1 );
    obj.value   = MIDI.normalizeData   ( parseInt(csv[4]) ); 
    obj.articulationID = normalizeArtId ( csv, 5);
    obj.port     = normalizePort        ( csv, 6);
    midiBuffer.push(obj);
}

function addPolyPressure(csv) {
    let obj = new PolyPressure;
    obj.beatPos = ticks2Beats(csv[1]);
    obj.channel = MIDI.normalizeChannel( parseInt(csv[3])+1 );
    obj.pitch   = MIDI.normalizeData   ( parseInt(csv[4]) ); 
    obj.value   = MIDI.normalizeData   ( parseInt(csv[5]) );
    obj.articulationID = normalizeArtId ( csv, 6);
    obj.port     = normalizePort        ( csv, 7);
    midiBuffer.push(obj);
}

function normalizeArtId(csv, pos) {
    let id = 0;
    if(csv.length > pos ) {
        id = parseInt(csv[pos]);
        if( id < 0 || id > 254 ) id = 0;
    }
    return id;
}

// what is the actual limit?  48 is the limit of VePro
function normalizePort(csv, pos) {
    let val = 1;
    if(csv.length > pos) {
        val = parseInt(csv[pos])+1;
        if (val < 1) val = 1;
        else if (val > 48) val=48;
    }
    return val;
}

function normalizePitchBend(strVal) {
    // convert from midifile range 0-16383 to -8192 - 8191
    let val = parseInt(strVal)-8192;
    if(val < -8192) val = -8192
    else if (val > 8191) val = 8191;
    return val;
}

/**************************************
 * convert midifile ticks to LPX beats
 * TODO, normalize it?
 **************************************/

function ticks2Beats(ticks) {
    return (parseInt(ticks)/PPQN ) + 1;
}

/**************************************
 * buffered logging in case we need
 **************************************/

var console = {
    maxFlush: 20,
    b: [],
    log: function(msg) {
        this.b.push(msg)
    },
    flush: function() {
        var i = 0;
        while (i <= this.maxFlush && this.b.length > 0) {
            Trace(this.b.shift());
            i++;
        }
    }
};

/**************************************
 * Idle Callback
 **************************************/
/*
function Idle() {
    console.flush();
}
*/


