/******************************************************************
 *
 *  Micro Quantizer
 *
 *  version 1.0.0
 *
 *  EXPERIMENTAL 
 *
 *  micro quantizer modies streamed midi notes to different PPQN
 *  This is experimental and there is at least one problem which is
 *  that beatPos can often only be rounded up and not down, due to
 *  process block boundary.  So its impperfect.  Use the midicsv
 *  exporter to get a true micro-quantized result.
 ******************************************************************/


/******************************************************************
 ********* DO NOT EDIT BELOW HERE *********************************
 ******************************************************************/

var NeedsTimingInfo = true;

var PPQN = 960; // init later from GUI

var lastTimeStamp = 0;


/***************************
 * HandleMIDI
 ***************************/

function HandleMIDI(event) {

    var info = GetTimingInfo();
    if (!info.playing) {
        event.send();
        return;
    }

    let ticks = Math.round((event.beatPos - 1) * PPQN);
    event.beatPos = 1 + (ticks / PPQN); //convert after rounding
    event.sendAtBeat(event.beatPos);
}



function Reset() {

    PPQN = pdata[GuiParameter(0)];

} // end Reset()


/****************
 * GUI
 ****************/

var PluginParameters = [];

PluginParameters.push({
    name: "Output PPQN",
    type: "menu",
    valueStrings: ["3840", "3072", "1920", "1536", "960", "768", 
                   "480", "384", "240", "192", "120", "96", "48", "24"],
    defaultValue: 4,
    disableAutomation: true,
    hidden: false
});
var pdata = [3840, 3072, 1920, 1536, 960, 768, 
             480, 384, 240, 192, 120, 96, 48, 24];


function ParameterChanged(id, val) {
    PluginParameters[id].data = val;
}

// Faster function to get GUI value
function GuiParameter(id) {

    // just in case programmer error
    if (id >= PluginParameters.length) return undefined;

    // if script was recently initialized, reload GUI value
    if (PluginParameters[id].data == undefined) {
        PluginParameters[id].data = GetParameter(id);
    }

    return PluginParameters[id].data;
}

/*******************
 * buffered logging
 *******************/

var console = {
    maxFlush: 20,
    b: [],
    log: function(msg) {
        this.b.push(msg)
    },
    flush: function() {
        let i = 0;
        while (i <= this.maxFlush && this.b.length > 0) {
            Trace(this.b.shift());
            i++;
        }
    }
};

function Idle() {
    console.flush();
}
