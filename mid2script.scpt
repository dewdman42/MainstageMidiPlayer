

-- This script needs to be exported as an Application bundle, then you can drag and 
-- drop midifiles onto it and the Scripter code will end up on the clipboard to directly 
-- paste in Scripter.  Make sure that inside this bundle you also have midicsv and MidiPlayer.js
-- saved in the Resources folder


on open theDroppedItems
	
	set midicsv to POSIX path of (path to resource "midicsv")
	set player to POSIX path of (path to resource "MidiPlayer.js")
	
	repeat with i from 1 to length of theDroppedItems
		set currentItem to item i of theDroppedItems
		set fileName to POSIX path of currentItem
		
		set command to "cat " & player
		set result1 to do shell script (command)
		
		set command to midicsv & " " & fileName
		set result2 to do shell script (command)
		
		set finalResults to result1 & return & "var MidiSequence = `" & result2 & "`"
		
		set the clipboard to finalResults
	end repeat
end open



