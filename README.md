# Mainstage/LogicPro Midi Player

## Description

This repo contains some javascript tools to be used in __LogicPro/Mainstage Scripter__ for providing midi sequence playback in __Apple Mainstage and LogicPro__; and for generating std midifiles that can be used elsewhere.  Various other related tools may appear here over time.


More Info and usage DOCUMENTATION found here: [Wiki](https://gitlab.com/dewdman42/MainstageMidiPlayer/-/wikis/home)