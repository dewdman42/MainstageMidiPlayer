/******************************************************************
 *
 *  LogicGenCsv - midicsv generator
 *
 *  version 1.0.6
 *
 * This script will generate a midicsv output, which can be copy and 
 * pasted, then sent to the csvmidi program to produce a midifile.
 * It can also be used to generate output that can be used inside
 * the MidiPlayer.js script to playback midi in Mainstage.
 *
 * LogicPro has some serious bugs in how it exports midfiles, including
 * it only exports at 480 ppqn and doesn't appear to even always do that
 * correctly when notes are 1 tick apart.  So this script will export
 * them, AND, it will have the option to apply micro quantizing to 
 * any new PPQN that is desired also in the process.
 *
 ******************************************************************/


/******************************************************************
 ********* DO NOT EDIT BELOW HERE *********************************
 ******************************************************************/

var NeedsTimingInfo = true;

var PPQN = 960; // init later from GUI

var lastTimeStamp = 0;
var lastTempo = 0;
var lastMeterNum = 0;
var lastMeterDem = 0;

var started = true;

/***************************
 * HandleMIDI
 ***************************/

function HandleMIDI(event) {

    if(started==false) {
        event.send();
        return;
    }

    var info = GetTimingInfo();
    if(info.playing==false) {
        return;
    }

    checkTempo(info);
    checkMeter(info);

    event.ticks = Math.round((event.beatPos - 1) * PPQN);
    event.beatPos = 1 + (event.ticks / PPQN); //convert after rounding
    event.sendAtBeat(event.beatPos);

    lastTimeStamp = event.ticks;

    if(typeof event.articulationID == 'undefined') event.articulationID = 0;
    if(typeof event.port == 'undefined') event.port = 0;

    let menu = GuiParameter(1);
    if(menu == 0 || menu == event.channel) {
        console.log(event.format());
    }
}

/*******************************************************
 * ProcessMIDI
 * Used to detect PLAY state
 *******************************************************/
var isPlaying = false;

function ProcessMIDI() {

    if(started==false) return;

    info = GetTimingInfo();

    // IF IS PLAYING NOW, set flag
    if (info.playing) {
        isPlaying = true;
        checkTempo(info);
        checkMeter(info);
    }

    // STOPPED NOW
    else {
        // WAS IT RECENTLY PLAYING?
        if (isPlaying == true) {
            handleStop();
            isPlaying = false;
        }
        started=false;
    }
}



/*******************************************************
 * format() provided for each event type
 *******************************************************/

NoteOn.prototype.format = function() {
    let str = `1, ${this.ticks}, Note_on_c, ${this.channel-1}, ` +
              `${this.pitch}, ${this.velocity}`;
    if( GuiParameter(2) == 1) {
        str += `, ${this.articulationID}, ${this.port-1}`;
    }
    return str;
};

NoteOff.prototype.format = function() {
    let str = `1, ${this.ticks}, Note_off_c, ${this.channel-1}, ` +
              `${this.pitch}, ${this.velocity}`;
    if( GuiParameter(2) == 1) {
        str += `, ${this.articulationID}, ${this.port-1}`;
    }
    return str;
};

ControlChange.prototype.format = function() {
    let str = `1, ${this.ticks}, Control_c, ${this.channel-1}, ` +
        `${this.number}, ${this.value}`;
    if( GuiParameter(2) == 1) {
        str += `, ${this.articulationID}, ${this.port-1}`;
    }
    return str;
};

PitchBend.prototype.format = function() {
    let str = `1, ${this.ticks}, Pitch_bend_c, ${this.channel-1}, ` +
              `${this.value+8192}`;
    if( GuiParameter(2) == 1) {
        str += `, ${this.articulationID}, ${this.port-1}`;
    }
    return str;
};

ProgramChange.prototype.format = function() {
    let str = `1, ${this.ticks}, Program_c, ${this.channel-1}, ` +
              `${this.number}`;
    if( GuiParameter(2) == 1) {
        str += `, ${this.articulationID}, ${this.port-1}`;
    }
    return str;
};

ChannelPressure.prototype.format = function() {
    let str = `1, ${this.ticks}, Channel_aftertouch_c, ${this.channel-1}, ` +
        `${this.value}`;
    if( GuiParameter(2) == 1) {
        str += `, ${this.articulationID}, ${this.port-1}`;
    }
    return str;
};

PolyPressure.prototype.format = function() {
    let str = `1, ${this.ticks}, Poly_aftertouch_c, ${this.channel-1}, ` +
        `${this.pitch}, ${this.value}`;
    if( GuiParameter(2) == 1) {
        str += `, ${this.articulationID}, ${this.port-1}`;
    }
    return str;
};



function Reset() {

    started = true;
    PPQN = pdata[GuiParameter(0)];
    lastTimeStamp = 0;
    lastTempo = 0;
    lastMeterNum = 0;
    lastMeterDem = 0;

    // TODO, get more meta information like title and other things?

    console.log(`
#***********************************************************
#***************** CONVERT WITH csvmidi ********************
#***********************************************************
0, 0, Header, 0, 1, ${PPQN}
1, 0, Start_track
1, 0, Title_t, "LogicGenCsv"`);

} // end Reset()


/******************************
 * Output end of midifile
 ******************************/

function handleStop() {

    let endTime = lastTimeStamp;
    if(GuiParameter(3) == 1) {
        lastTimeStamp = GuiParameter(4) * PPQN;
    }
    console.log(`#***************************************
1, ${lastTimeStamp}, End_track
0, 0, End_of_file
#****************************************

`);

}


/*******************************************************
 * If meter changed, send data
 *******************************************************/

function checkMeter(info) {
    if (info.meterNumerator != lastMeterNum || info.meterDenominator != lastMeterDem) {
        console.log(`1, ${lastTimeStamp}, Time_signature, ` +
            `${info.meterNumerator}, ${info.meterDenominator}, 24, 8`);

        lastMeterNum = info.meterNumerator;
        lastMeterDem = info.meterDenominator;
    }
}

/*******************************************************
 * If tempo changed, send data
 *******************************************************/

function checkTempo(info) {
    if (info.tempo != lastTempo) {
        console.log(`1, ${lastTimeStamp}, `
                +`Tempo, ${Math.round(60000000/info.tempo)}`);
        lastTempo = info.tempo;
    }
}


/****************
 * GUI
 ****************/

var PluginParameters = [];

PluginParameters.push({
    name: "Output PPQN",
    type: "menu",
    valueStrings: ["32767","15360","7680", "3840", "3072","1920","1536",
                "960","768","512 (2048th)","480", "384","256 (1024th)",
                "240", "192","128 (512th)","120","96","64 (256th)","48",
                "32 (128th)","24"],
    defaultValue: 7,
});
var pdata = [32767, 15360,7680, 3840, 3072, 1920, 1536, 960, 
             768, 512, 480, 384, 256, 240, 192, 128, 120, 96,
             64, 48, 32, 24];

// Note that 32767 is I believe the largest value that should be
// used for PPQN because any value higher then that will overlap into 
// values that are meant for SMPTE time division, which midicsv doesn't 
// currently support, most DAW's don't export that format either and
// The entire approach here would have to be changed to make calculations
// base on SMPTE frame rates and other such things.  Might be a fun project
// some day to see if any other programs could even be able to import it.



PluginParameters.push({
    name: "Midi Channel",
    type: "menu",
    valueStrings: ["ALL","1","2","3","4","5","6","7","8",
                  "9","10","11","12","13","14","15","16"],
    defaultValue: 0,
});


PluginParameters.push({
    name: "ArtID and Port",
    type: "checkbox",
    defaultValue: 0
});

PluginParameters.push({
    name: "Midi END BEAT",
    type: "checkbox",
    defaultValue: 0
});

PluginParameters.push({
    name: "EndBeat",
    type: "lin",
    minValue: 1,
    maxValue: 1000,
    numberOfSteps: 999,
    defaultValue: 5,
});



function ParameterChanged(id, val) {
    PluginParameters[id].data = val;
}

// Faster function to get GUI value
function GuiParameter(id) {

    // just in case programmer error
    if (id >= PluginParameters.length) return undefined;

    // if script was recently initialized, reload GUI value
    if (PluginParameters[id].data == undefined) {
        PluginParameters[id].data = GetParameter(id);
    }

    return PluginParameters[id].data;
}

/*******************
 * buffered logging
 *******************/

var console = {
    maxFlush: 20,
    b: [],
    log: function(msg) {
        this.b.push(msg)
    },
    flush: function() {
        let i = 0;
        while (i <= this.maxFlush && this.b.length > 0) {
            Trace(this.b.shift());
            i++;
        }
    }
};

function Idle() {
    console.flush();
}






