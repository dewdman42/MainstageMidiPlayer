/******************************************************************
 *
 *  LogicGen.js
 *
 *  version 1.0.0
 *
 *  use this script to generate data for midi sequence playback
 *  in Mainstage Scripter.  Copy and Paste the data structure logged  
 *  into the engine.js script, assigned to 
 *
 *  var MidiSequence = [
 *   {.......}
 *  ,{.......}
 *  ];
 *
 ******************************************************************/


/******************************************************************
 ********* DO NOT EDIT BELOW HERE *********************************
 ******************************************************************/

var NeedsTimingInfo = true;

/***************************
 * HandleMIDI
 ***************************/

function HandleMIDI(event) {

    event.send();

    var ctx = GetTimingInfo();
    if (!ctx.playing) {
        return;
    }

    let channel = GuiParameter(1);

    if (channel == 0 || channel == event.channel) {
        console.log(event.format());
    }

    event.send();
}

/*******************************************************
 * format() provided for each event type
 *******************************************************/

NoteOn.prototype.format = function() {
    let str = showComma() +
        "{type:\'NoteOn\',         " + this.padBeat() +
        this.padChannel() +
        this.showPitch() +
        this.padVelocity();

    if (this.articulationID != undefined && this.articulationID > 0) {
        str += this.padArtId();
    }
    if (this.port != undefined && this.port > 1) {
        str += this.padPort();
    }
    str = str.substr(0, str.length - 1);
    str += "}";
    return str;
};

NoteOff.prototype.format = function() {
    let str = showComma() +
        "{type:\'NoteOff\',        " + this.padBeat() +
        this.padChannel() +
        this.showPitch() +
        this.padVelocity();

    if (this.articulationID != undefined && this.articulationID > 0) {
        str += this.padArtId();
    }
    if (this.port != undefined && this.port > 1) {
        str += this.padPort();
    }
    str = str.substr(0, str.length - 1);
    str += "}";
    return str;
};

ControlChange.prototype.format = function() {
    let str = showComma() +
        "{type:\'ControlChange\',  " + this.padBeat() +
        this.padChannel() +
        this.padNumber() +
        this.padValue()
    if (this.port != undefined && this.port > 1) {
        str += this.padPort();
    }
    str = str.substr(0, str.length - 1);
    str += "  }";
    return str;
};

PitchBend.prototype.format = function() {
    let str = showComma() +
        "{type:\'PitchBend\',      " + this.padBeat() +
        this.padChannel() +
        this.padValue();
    if (this.port != undefined && this.port > 1) {
        str += this.padPort();
    }
    str = str.substr(0, str.length - 1);
    str += "              }";
    return str;
};

ProgramChange.prototype.format = function() {
    let str = showComma() +
        "{type:\'ProgramChange\',  " + this.padBeat() +
        this.padChannel() +
        this.padNumber();
    if (this.port != undefined && this.port > 1) {
        str += this.padPort();
    }
    str = str.substr(0, str.length - 1);
    str += "               }";
    return str;
};

ChannelPressure.prototype.format = function() {
    let str = showComma() +
        "{type:\'ChannelPressure\'," + this.padBeat() +
        this.padChannel() +
        this.padValue();
    if (this.port != undefined && this.port > 1) {
        str += this.padPort();
    }
    str = str.substr(0, str.length - 1);
    str += "              }";
    return str;
};

PolyPressure.prototype.format = function() {
    let str = showComma() +
        "{type:\'PolyPressure\',   " + this.padBeat() +
        this.padChannel() +
        this.showPitch() +
        this.padValue();
    if (this.port != undefined && this.port > 1) {
        str += this.padPort();
    }
    str = str.substr(0, str.length - 1);
    str += " }";
    return str;
};


Event.prototype.padBeat = function() {
    let str = Number.parseFloat(this.beatPos)
        .toFixed(6) + ",";
    str = "beatPos:" + str.padStart(11);
    return str;
};

Event.prototype.padChannel = function() {
    let str = this.channel.toString() + ",";
    str = " channel:" + str.padStart(3);
    return str;
};

Event.prototype.padVelocity = function() {
    let str = this.velocity.toString() + ",";
    str = " velocity:" + str.padStart(4);
    return str;
};

Event.prototype.padArtId = function() {
    let str = this.articulationID.toString() + ",";
    str = " articulationID:" + str.padStart(4);
    return str;
};

Event.prototype.padPort = function() {
    let str = this.port.toString() + ",";
    str = " port:" + str.padStart(3);
    return str;
};

Event.prototype.padNumber = function() {
    let str = this.number.toString() + ",";
    str = " number:" + str.padStart(4);
    return str;
};

Event.prototype.padValue = function() {
    let str = this.value.toString() + ",";
    str = " value:" + str.padStart(6);
    return str;
};

Event.prototype.showPitch = function showPitch() {
    if (GuiParameter(0) == 0) {
        let str = "\'" + MIDI.noteName(this.pitch) + "\',";
        str = " pitch:" + str.padStart(6);
        return str;
    }
    else {
        let str = this.pitch.toString() + ",";
        str = " pitch:" + str.padStart(4);
        return str;
    }
};



/**************************
 * logging
 **************************/

function Reset() {
    console.log(`
/***********************************************************
 ******************* SEQUENCE DATA *************************
 ***********************************************************/
`);

    first = true;
}

var console = {
    maxFlush: 20,
    b: [],
    log: function(msg) {
        this.b.push(msg)
    },
    flush: function() {
        let i = 0;
        while (i <= this.maxFlush && this.b.length > 0) {
            Trace(this.b.shift());
            i++;
        }
    }
};

function Idle() {
    console.flush();
}

/**************
 * GUI
 **************/

var PluginParameters = [];
PluginParameters.push({
    name: "Pitch Values",
    type: "menu",
    valueStrings: ["By Name", "By Number"],
    defaultValue: 0,
    data: 0,
    hidden: false,
    disableAutomation: true
});

PluginParameters.push({
    name: "Midi Channel",
    type: "menu",
    valueStrings: ["ALL", "1", "2", "3", "4", "5", "6", "7", "8", 
                   "9", "10", "11", "12", "13", "14", "15", "16"],
    defaultValue: 0,
    data: 0,
    hidden: false,
    disableAutomation: true
});


/******************************
 * helper functions 
 ******************************/

var first = true;

function showComma() {
    if (first) {
        first = false;
        return " ";
    }
    else {
        return ",";
    }
}

function ParameterChanged(id, val) {
    PluginParameters[id].data = val;
}

// Faster function to get GUI value
function GuiParameter(id) {

    // just in case programmer error
    if (id >= PluginParameters.length) return undefined;

    // if script was recently initialized, reload GUI value
    if (PluginParameters[id].data == undefined) {
        PluginParameters[id].data = GetParameter(id);
    }

    return PluginParameters[id].data;
}
